#include <Arduino.h>
#include <AccelStepper.h>
#include <MultiStepper.h>
#include <setjmp.h>
jmp_buf main_switch_exit;
uint64_t last_main_switch_trigger = 0;

AccelStepper yaw(AccelStepper::DRIVER, 15, 14);
constexpr auto yaw_switch = A3;
constexpr auto yaw_en = 16;

AccelStepper pitch(AccelStepper::DRIVER, 10, 9);
constexpr auto pitch_switch = A2;
constexpr auto pitch_en = 8;

constexpr auto main_switch = A1;

constexpr auto valve = 6;

constexpr auto led_ok = 7;
constexpr auto led_fail = 5;

MultiStepper multi;

enum System_Status {
  INIT,
  READY,
  FAULT,
} volatile status;

void fault(const char* reason = NULL) {
  digitalWrite(yaw_en, HIGH);
  digitalWrite(pitch_en, HIGH);
  status = FAULT;
  if (reason) Serial.println(reason);
  Serial.println("FAULT");
}

bool switch_triggered(uint8_t pin) {
  return !digitalRead(pin);
}

void check_main_switch() {
  if (!switch_triggered(main_switch)) {
    last_main_switch_trigger = millis();
    longjmp (main_switch_exit, 1);
  }
}

void init_() {
  digitalWrite(yaw_en, LOW);
  digitalWrite(pitch_en, LOW);
  digitalWrite(valve, LOW);

  yaw.setSpeed(-100);
  pitch.setSpeed(-100);
  Serial.println("seeking yaw stop switch");
  auto start = millis();
  while(!switch_triggered(yaw_switch)) {
    check_main_switch();
    yaw.runSpeed();
    // if (millis() - start > 5000) {
    //   fault("yaw timeout");
    // }
  }


  Serial.println("seeking pitch stop switch");
  start = millis();
  while(!switch_triggered(pitch_switch)) {
    check_main_switch();
    pitch.runSpeed();
    // if (millis() - start > 5000) {
    //   fault("pitch timeout");
    // }
  }


  yaw.setCurrentPosition(0);
  yaw.setSpeed(1000);
  pitch.setCurrentPosition(0);
  pitch.setSpeed(1000);
  status = READY;
  Serial.println("READY");
  while(Serial.available()) {
    Serial.read();
  }
}

void move(long y, long p) {
  long positions[] = {y,p};
  multi.moveTo(positions);
  while (multi.run()) {
    check_main_switch();
    // if (switch_triggered(yaw_switch)) {
    //   fault("yaw switch triggered");
    // }
    // if (switch_triggered(pitch_switch)) {
    //   fault("pitch switch triggered");
    // }
  };
}

void open_valve(unsigned long duration) {
  auto start = millis();
  digitalWrite(valve, HIGH);
  while((millis() - start) < duration) {
    check_main_switch();
  }
  digitalWrite(valve, LOW);
}

void ready() {
  if (Serial.available()) {
    char buf[128];
    auto pos = Serial.readBytesUntil('\n', buf, 127);
    buf[pos] = 0;
    long x, y;
    if (sscanf(buf, "move %ld %ld", &x, &y) == 2) {
      Serial.print("moving to ");
      Serial.print(" ");
      Serial.print(x);
      Serial.print(" ");
      Serial.print(y);
      Serial.println();
      move(x, y);
      Serial.println("READY");
    } else if (sscanf(buf, "valve %ld", &x) == 1) {
      Serial.print("opening valve for ");
      Serial.print(x);
      Serial.println("ms");
      open_valve(x);
      Serial.println("READY");
    } else if (strncmp(buf, "reset", 5) == 0) {
      Serial.print("resetting");
      status = INIT;
    } else {
      Serial.print("wrong command ");
      Serial.println(buf);
    }
  }
}

void setup() {
  pinMode(led_ok, OUTPUT);
  pinMode(led_fail, OUTPUT);

  digitalWrite(valve, LOW);
  pinMode(valve, OUTPUT);
  Serial.begin(9600);
  while(!Serial);
  multi.addStepper(yaw);
  multi.addStepper(pitch);

  pinMode(yaw_en, OUTPUT);
  pinMode(pitch_en, OUTPUT);
  digitalWrite(yaw_en, HIGH);
  digitalWrite(pitch_en, HIGH);
  
  pinMode(yaw_switch, INPUT_PULLUP);
  pinMode(pitch_switch, INPUT_PULLUP);
  pinMode(main_switch, INPUT_PULLUP);
  yaw.setMaxSpeed(1000);
  pitch.setMaxSpeed(1000);

  if (setjmp (main_switch_exit)) {
    Serial.println("OPEN");
    digitalWrite(led_ok, LOW);
    auto last_trigger = millis();
    while((millis() - last_trigger) < 1000) {
      digitalWrite(led_fail, millis() % 1000 > 500);
      if (!switch_triggered(main_switch)) {
        last_trigger = millis();
      }
    };
  }
  status = INIT;
  while(true) {
    check_main_switch();
    switch (status)
    {
    case INIT:
      digitalWrite(led_ok, LOW);
      digitalWrite(led_fail, LOW);
      init_();
      break;
    
    case FAULT:
      digitalWrite(led_ok, LOW);
      digitalWrite(led_fail, HIGH);
      break;

    case READY:
      digitalWrite(led_ok, HIGH);
      digitalWrite(led_fail, LOW);
      ready();
      break;
    }
  }
}




void loop() {}